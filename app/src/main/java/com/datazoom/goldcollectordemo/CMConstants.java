package com.datazoom.goldcollectordemo;

public class CMConstants {
    public static final int ADD_APP_SESSION_METADATA = 1;
    public static final int ADD_PLAYER_SESSION_METADATA = 2;

    public static final int READ_APP_SESSION_METADATA = 3;
    public static final int READ_PLAYER_SESSION_METADATA = 4;


    public static final int DELETE_APP_SESSION_METADATA = 5;
    public static final int DELETE_PLAYER_SESSION_METADATA = 6;


    public static final int UPDATE_APP_SESSION_METADATA = 7;
    public static final int UPDATE_PLAYER_SESSION_METADATA = 8;

    public static final int ADD_CUSTOM_METADATA_FOR_EXPLICINT_EVENT = 9;
    public static final int DELETE_CUSTOM_METADATA_FOR_EXPLICINT_EVENT = 10;


}
