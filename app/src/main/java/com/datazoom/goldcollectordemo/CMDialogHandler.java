package com.datazoom.goldcollectordemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dz.collector.android.collector.DZEventCollector;
import com.dz.collector.android.eventtypes.EventType;

import java.util.HashMap;

public class CMDialogHandler {

    private static AlertDialog addAppSessionMetadata;

    public static void addAppSessionMetadataDialog(final Activity activity) {
        if (addAppSessionMetadata != null) {
            addAppSessionMetadata.dismiss();
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_metadata_dialog, null);

        EditText key_1 = dialogView.findViewById(R.id.key_1);
        EditText value_1 = dialogView.findViewById(R.id.value_1);


        Button addMetadataButton = dialogView.findViewById(R.id.addMetadataButton);
        addMetadataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!key_1.getText().toString().isEmpty() && !value_1.getText().toString().isEmpty()) {
                    HashMap<String, Object> appSessionMapTest = new HashMap<>();
                    appSessionMapTest.put(key_1.getText().toString(), value_1.getText().toString());
                    DZEventCollector.setAppSessionCustomMetadata(appSessionMapTest);
                    addAppSessionMetadata.dismiss();
                    addAppSessionMetadata = null;
                } else {
                    Toast.makeText(activity, activity.getString(R.string.please_insert_key_value_pair), Toast.LENGTH_LONG).show();
                }
            }
        });
        Button cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAppSessionMetadata.dismiss();
                addAppSessionMetadata = null;
            }
        });


        dialogBuilder.setView(dialogView);
        addAppSessionMetadata = dialogBuilder.create();
        addAppSessionMetadata.setCancelable(false);
        addAppSessionMetadata.show();
    }


    private static AlertDialog addPlayerSessionMetadata;

    public static void addPlayerSessionMetadataDialog(final Activity activity) {
        if (addPlayerSessionMetadata != null) {
            addPlayerSessionMetadata.dismiss();
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_metadata_dialog, null);

        EditText key_1 = dialogView.findViewById(R.id.key_1);
        EditText value_1 = dialogView.findViewById(R.id.value_1);

        Button addMetadataButton = dialogView.findViewById(R.id.addMetadataButton);
        addMetadataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!key_1.getText().toString().isEmpty() && !value_1.getText().toString().isEmpty()) {
                    HashMap<String, Object> appPlayerMapTest = new HashMap<>();
                    appPlayerMapTest.put(key_1.getText().toString(), value_1.getText().toString().isEmpty());
                    DZEventCollector.setPlayerSessionCustomMetadata(appPlayerMapTest);
                    addPlayerSessionMetadata.dismiss();
                    addPlayerSessionMetadata = null;
                } else {
                    Toast.makeText(activity, activity.getString(R.string.please_insert_key_value_pair), Toast.LENGTH_LONG).show();
                }
            }
        });

        Button cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPlayerSessionMetadata.dismiss();
                addPlayerSessionMetadata = null;
            }
        });
        dialogBuilder.setView(dialogView);
        addPlayerSessionMetadata = dialogBuilder.create();
        addPlayerSessionMetadata.setCancelable(false);
        addPlayerSessionMetadata.show();
    }

    private static AlertDialog updateAppSessionMetadata;
    public static void updateAppSessionMetadataDialog(final Activity activity) {
        if (updateAppSessionMetadata != null) {
            updateAppSessionMetadata.dismiss();
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_metadata_dialog, null);

        EditText key_1 = dialogView.findViewById(R.id.key_1);
        EditText value_1 = dialogView.findViewById(R.id.value_1);

        Button addMetadataButton = dialogView.findViewById(R.id.addMetadataButton);
        addMetadataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!key_1.getText().toString().isEmpty() && !value_1.getText().toString().isEmpty()) {
                    DZEventCollector.updateAppSessionCustomMetadata(key_1.getText().toString(), value_1.getText().toString());
                    updateAppSessionMetadata.dismiss();
                    updateAppSessionMetadata = null;
                } else {
                    Toast.makeText(activity, activity.getString(R.string.please_insert_key_value_pair), Toast.LENGTH_LONG).show();
                }
            }
        });

        Button cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateAppSessionMetadata.dismiss();
                updateAppSessionMetadata = null;
            }
        });
        dialogBuilder.setView(dialogView);
        updateAppSessionMetadata = dialogBuilder.create();
        updateAppSessionMetadata.setCancelable(false);
        updateAppSessionMetadata.show();
    }

    private static AlertDialog updatePalyerSessionMetadata;
    public static void updatePlayerSessionMetadataDialog(final Activity activity) {
        if (updatePalyerSessionMetadata != null) {
            updatePalyerSessionMetadata.dismiss();
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_metadata_dialog, null);

        EditText key_1 = dialogView.findViewById(R.id.key_1);
        EditText value_1 = dialogView.findViewById(R.id.value_1);

        Button addMetadataButton = dialogView.findViewById(R.id.addMetadataButton);
        addMetadataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!key_1.getText().toString().isEmpty() && !value_1.getText().toString().isEmpty()) {
                    DZEventCollector.updatePlayerSessionCustomMetadata(key_1.getText().toString(), value_1.getText().toString());
                    updatePalyerSessionMetadata.dismiss();
                    updatePalyerSessionMetadata = null;
                } else {
                    Toast.makeText(activity, activity.getString(R.string.please_insert_key_value_pair), Toast.LENGTH_LONG).show();
                }
            }
        });

        Button cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePalyerSessionMetadata.dismiss();
                updatePalyerSessionMetadata = null;
            }
        });
        dialogBuilder.setView(dialogView);
        updatePalyerSessionMetadata = dialogBuilder.create();
        updatePalyerSessionMetadata.setCancelable(false);
        updatePalyerSessionMetadata.show();
    }

    private static AlertDialog metadataForExplicitEventDialog;
    public static void addMetadataForExplicitEventDialog(final Activity activity) {
        if (metadataForExplicitEventDialog != null) {
            metadataForExplicitEventDialog.dismiss();
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_metadata_dialog, null);

        EditText key_1 = dialogView.findViewById(R.id.key_1);
        EditText value_1 = dialogView.findViewById(R.id.value_1);

        Button addMetadataButton = dialogView.findViewById(R.id.addMetadataButton);
        addMetadataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!key_1.getText().toString().isEmpty() && !value_1.getText().toString().isEmpty()) {
                    HashMap<String, Object> testPauseMap = new HashMap<>();
                    testPauseMap.put(key_1.getText().toString(), value_1.getText().toString());
                    DZEventCollector.addMetadataForAppropriateEvent(EventType.PAUSE, testPauseMap);
                    metadataForExplicitEventDialog.dismiss();
                    metadataForExplicitEventDialog = null;
                } else {
                    Toast.makeText(activity, activity.getString(R.string.please_insert_key_value_pair), Toast.LENGTH_LONG).show();
                }
            }
        });

        Button cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metadataForExplicitEventDialog.dismiss();
                metadataForExplicitEventDialog = null;
            }
        });
        dialogBuilder.setView(dialogView);
        metadataForExplicitEventDialog = dialogBuilder.create();
        metadataForExplicitEventDialog.setCancelable(false);
        metadataForExplicitEventDialog.show();
    }
}
